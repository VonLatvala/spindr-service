status: HTTP Response code
results: Array
    bio: String
    birth_date: String (https://www.w3.org/TR/NOTE-datetime)
    birth_date_info: String (irrelevant)
    common_friend_count: Int
    common_friends: Array(String) # a number
    common_like_count: Int
    common_likes: Array(String) # a number
    connection_count: Int
    content_hash: String (SHA1?)
    distance_mi: Int
    gender: Int (1=f or 0=m)
    instagram: Map
        completed_final_fetch: bool
        last_fetch_time: String (https://www.w3.org/TR/NOTE-datetime)
        media_count: Int
        photos: Array
            image: String (URL to instagram)
            link: String (URL to instagram)
            thumbnail: String (URL to instagram)
            ts: String (a number?)
        profile_picture: String (URL to instagram image)
        username: String
    jobs: Array
        company: Map
            name: String
        title: Map
            name: String
    name: String
    photos: Array
        id: String (UUID?)
        processedFiles: Array
            height: Int
            url: String
            width: Int
        url: String (URL to images.gotinder.com)
    ping_time: String (https://www.w3.org/TR/NOTE-datetime)
    s_number: Int (???)
    schools: Array
        id: String # a number
        name: String
    teaser: Map
        string: String
    teasers: Array
        string: String
        type: String
    _id: String (short hash?)

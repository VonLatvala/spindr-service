#!/usr/bin/env python3

import sys
from spindr import SpindRApp
from spindr import TinderUser
import pprint
import json
from time import sleep
from time import time

if len(sys.argv) > 1:
    app = SpindRApp()
    cmd = sys.argv[1]
    if cmd == "get_recs":
        if not app.is_authorized():
            if app.tinder_auth():
                app.run_continuous_spider()
            else:
                print("Couldn't auth with tinder.")
    elif cmd == "parse_local":
        app.parse_local()
    elif cmd == "download_local":
        app.download_local()
    elif cmd == "meta":
        if not app.is_authorized():
            if app.tinder_auth():
                pp = pprint.PrettyPrinter(indent=4)
                res = app.get_meta()
                pp.pprint(res)
                with open("tmp/meta.json", "w") as fd:
                    fd.write(json.dumps(res))
            else:
                print("Couldn't auth with tinder.")
    elif cmd == "init_db":
        app.init_sqlite()
    elif cmd == "enum_user_ids":
        for row in app.load_user_ids():
            for field, value in enumerate(row):
                print("{}: {}".format(field, value))
    elif cmd == "select_user_with_id":
        user = TinderUser()
        user.load_with_id(sys.argv[2])
        user.dump_json()
    elif cmd == "dump_users_in_sql":
        user_ids = app.get_user_ids()
        for user_id_row in user_ids:
            user = TinderUser()
            user.load_with_id(user_id_row["id"])
            user.dump_json()
    elif cmd == "run_like_machine":
        while True:
            if not app.is_authorized():
                if app.tinder_auth():
                    print("Authorized.")
                else:
                    raise ValueError("Unauthorized")
            app.update_likes_left()
            app.run_likemachine()
            sleep_sec = app.rate_limited_until/1000-time() + 10
            if sleep_sec < 0:
                sleep_sec = 60*60
            print("Done liking for now, sleeping {}sec".format(sleep_sec))
            sleep(sleep_sec)
    elif cmd == "likes_left":
        if not app.is_authorized():
            if app.tinder_auth():
                app.update_likes_left()
            else:
                print("Couldn't auth with tinder")
    elif cmd == "like":
        if not app.is_authorized():
            if app.tinder_auth():
                print("Authorized.")
            else:
                raise ValueError("Unauthorized")
        app.update_likes_left()
        if(app.likes_left > 0):
            app.vote_l(sys.argv[2])
    elif cmd == "mail":
        tinder_user = TinderUser()
        tinder_user.load_with_tinder_id(sys.argv[2])
        tinder_user.mail_user()
    elif cmd == "ping":
        if not app.is_authorized():
            if app.tinder_auth():
                print("Authorized.")
            else:
                raise ValueError("Unauthorized")
        app.ping(sys.argv[2], sys.argv[3])
    elif cmd == "save_recs_from_file":
        app.save_rec_from_file(sys.argv[2])
    elif cmd == "run_full_liker":
        if not app.is_authorized():
            if app.tinder_auth():
                print("Authorized.")
                app.rec_like_machine()
            else:
                raise ValueError("Unauthorized")
    else:
        print("Didn't understand command '{}'".format(cmd))
else:
    print("Please supply command")

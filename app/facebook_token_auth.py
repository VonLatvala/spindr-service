import re
import robobrowser
import requests
import json
import os.path

MOBILE_USER_AGENT_FILE = "config/mobileUserAgent.txt"
FACEBOOK_AUTH_URL_FILE = "config/fbToken.url"

class FacebookTokenAuth:
    """A class to encapsulate the retrieval of facebook auth token"""

    mobile_user_agent = None
    fb_auth_url = None
    fb_access_token = None
    fb_access_token_path = "data/fb_auth_token.txt"
    fb_credential_file_path_text = "config/facebookCredentials.json"
    fb_user_id = None

    def __init__(self):
        with open(MOBILE_USER_AGENT_FILE) as dataFile:
            self.mobile_user_agent = dataFile.read().replace("\n", "")
        with open(FACEBOOK_AUTH_URL_FILE) as dataFile:
            self.fb_auth_url = dataFile.read().replace("\n", "")
        if self.has_token_in_file():
            self.fb_access_token = self.read_fb_access_token_from_file()
        else:
            self.fb_access_token = self.retrieve_fb_access_token()
            self.write_fb_access_token_to_file(self.fb_access_token)
        self.fb_user_id = self.get_fb_user_id(self.fb_access_token)

    def get_fb_access_token(self, email, password):
        s = robobrowser.RoboBrowser(user_agent=self.mobile_user_agent, parser="lxml")
        s.open(self.fb_auth_url)
        f = s.get_form()
        f["pass"] = password
        f["email"] = email
        s.submit_form(f)
        f = s.get_form()
        s.submit_form(f, submit=f.submit_fields["__CONFIRM__"])
        access_token = re.search(r"access_token=([\w\d]+)", s.response.content.decode()).groups()[0]
        return access_token

    def get_fb_user_id(self, access_token):
        """Gets facebook ID from access token"""
        req = requests.get("https://graph.facebook.com/me?access_token=" + access_token)
        return req.json()["id"]

    def construct_tinder_auth_post_data(self):
        if not self.has_token():
            raise ValueError("Does not have facebook access token")
        if self.fb_user_id is None:
            raise ValueError("Does not have facebook user ID")
        return json.dumps({
            "facebook_token": self.fb_access_token,
            "facebook_id": self.fb_user_id
        })

    def has_token(self):
        if self.fb_access_token is None:
            return False
        else:
            return True

    def exists_token_file(self):
        if os.path.isfile(self.fb_access_token_path):
            return True
        else:
            return False

    def has_token_in_file(self):
        if self.exists_token_file():
            token = self.read_fb_access_token_from_file()
            if self.is_valid_fb_access_token(token):
                return True
            else:
                return False
        else:
            return False

    def read_fb_access_token_from_file(self):
        fd = open(self.fb_access_token_path)
        token = fd.readline().replace("\n", "").replace(" ", "")
        if not self.is_valid_fb_access_token(token):
            raise AuthorizationError("Invalid fb access token in file")
        return token


    def retrieve_fb_access_token(self):
        fb_auth_data = self.load_fb_credentials()
        token = self.get_fb_access_token(fb_auth_data["email"],
                                      fb_auth_data["password"])
        if not self.is_valid_fb_access_token(token):
            raise AuthorizationError("Couldn't get facebook access token")
        else:
            return token

    def is_valid_fb_access_token(self, token):
        if token is None:
            return False
        if len(token.replace(" ", "").replace("\n", "")) < 1:
            return False
        else:
            return True

    def load_fb_credentials(self):
        with open(self.fb_credential_file_path_text) as data_file:
            fb_auth_data = json.load(data_file)
        return fb_auth_data

    def write_fb_access_token_to_file(self, token):
        fd = open(self.fb_access_token_path, "w")
        fd.write(token)
        fd.close()

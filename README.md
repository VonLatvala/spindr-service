# SpindR

## Synopsis

**SpindR** is a tinder spider, which will feature a web-interface for tinder, as well as an autoliker. SpindR will build a database of all the people it finds on tinder, as well as use an upcomming project **DoxR** to dox your matches as well as possible.

## Installation

This project is still in alpha, and is not supposed to be used yet.

`git clone git clone https://bitbucket.org/axellatvala/spindr-service.git && python virtualenv -p python3 spindr-service && cd spindr-service && . bin/activate && pip3 install robobrowser requests lxml python-dateutil && vi app/config/facebookCredentials.json && cd app && ./cli.py init_database`

## Likemachine

`cd app && ./cli.py run_like_machine`

If you don't have a facebookToken.txt in app/config/, you will need to have a facebookCredentials.json in app/config/ containing the following information.

`{
    "email": "user@example.com",
    "password": "pass"
}`

Required libs: `pip3 install robobrowser requests lxml python-dateutil`

## Tests

Tests will be added later.

## Contributors

Axel Latvala

## License

For the time being, **SpindR** is UNLICENSED, and the descision for licensing is yet to be made, but will probably be BSD.


#!/usr/bin/env python3
import os.path
import json
import time
import sys
import pprint
import sqlite3

import requests

import dateutil.parser

from facebook_token_auth import FacebookTokenAuth
from time import sleep
from mailer import send_mail_with_images

DO_LOG = True
LOG_TO_STDOUT = True
LOG_PATH = "data/main.log"
DATE_SEPARATOR = "."
STRFTIME_DATE = "%d{}%m{}%Y".format(DATE_SEPARATOR, DATE_SEPARATOR)
TIME_SEPARATOR = ":"
STRFTIME_TIME = "%H{}%M{}%S".format(TIME_SEPARATOR, TIME_SEPARATOR)
DB_PATH = "data/spindr.db"
SQL_PATH = "sql"
INIT_DB_PATH = "{}/initDb".format(SQL_PATH)

def write_log(text):
    log_message = "[{}] {}\n".format(
        time.strftime("{} {}".format(STRFTIME_DATE, STRFTIME_TIME)), text)
    if DO_LOG:
        with open(LOG_PATH, "a") as fd:
            fd.write(log_message)
    if LOG_TO_STDOUT:
        sys.stdout.write(log_message)

class SpindRApp:

    tinder_auth_token = None
    tinder_auth_token_file_path = "data/tinder_auth_token.txt"
    imagedir = "data/images"
    dbdir = "data/db"
    rec_reqs_path = "data/recs.json"

    def __init__(self):
        self.setup_env()
        write_log("Initializing")
        with open("config/tinderAPI.url") as data_file:
            self.tinder_api_url = data_file.readline().replace("\n", "")
        with open("config/requiredHeaders.json") as data_file:
            self.base_headers = json.load(data_file)
        write_log("Started SpindR")
        self.likes_left = -1
        self.rate_limited_until = time.time()*1000

    def setup_env(self):
        if not os.path.exists("data"):
            os.makedirs("data")
        if not os.path.exists(self.dbdir):
            os.makedirs(self.dbdir)
        if not os.path.exists(self.imagedir):
            os.makedirs(self.imagedir)


    def tinder_auth(self):
        write_log("Authorizing with the tinder API")
        if self.has_tinder_auth_token_in_cache():
            self.tinder_auth_token = self.read_tinder_auth_token_from_cache()
            write_log("Had tinder auth token in cache. Validating...")
            req = requests.get(self.tinder_api_url)
            if req.status_code is 200:
                write_log("Token validated.")
                return True
            else:
                write_log("Invalid token, reauthorizing.")

        self.facebookTokenAuth = FacebookTokenAuth()
        tinder_auth_data = self.facebookTokenAuth.construct_tinder_auth_post_data()
        parsed_res = self.tinder_api_post("auth",
                                          self.base_headers, tinder_auth_data)
        if "token" in parsed_res:
            write_log("Successful authorization to tinder API")
            write_log("Tinder token: {}".format(parsed_res["token"]))
            self.tinder_auth_token = parsed_res["token"]
            self.update_tinder_auth_token_cache(self.tinder_auth_token)
            return True
        else:
            write_log("Failed authorizating to tinder API")
            write_log("Response: {}".format(parsed_res))
            return False

    def tinder_api_post(self, path, in_headers, in_data, ret_format="o"):
        #write_log("Posting {}\n{}".format(self.tinder_api_url+path, json.dumps(in_data)))
        req = requests.post(self.tinder_api_url+path,
                            headers=in_headers,
                            data=in_data)
        json_res = req.text
        if ret_format == "r":
            return json_res
        if ret_format == "o":
            if json_res == None:
                return {}
            try:
                return json.loads(json_res)
            except ValueError as e:
                write_log("Server responded incorrectly: {}".format(e))

    def tinder_api_post_preauth(self, path, data, ret_format="o"):
        return self.tinder_api_post(path, self.construct_tokenized_headers(), data,
                               ret_format)

    def tinder_api_get(self, path, in_headers, ret_format="o"):
        req = requests.get(self.tinder_api_url+path,
                           headers=in_headers)
        json_res = req.text
        #sys.stdout.write(json_res)
        if ret_format == "r":
            return json_res
        if ret_format == "o":
            try:
                return json.loads(json_res)
            except ValueError as e:
                write_log("Server responded incorrectly: {}".format(e))

    def tinder_api_get_preauth(self, path, ret_format="o"):
        return self.tinder_api_get(path, self.construct_tokenized_headers(),
                                   ret_format)


    def is_authorized(self):
        return self.tinder_auth_token is not None

    def failed_tinder_auth(self):
        facebookTokenAuth = FacebookTokenAuth()
        tinder_auth_data = json.loads(facebookTokenAuth.construct_tinder_auth_post_data())
        tinder_auth_data["facebook_token"] = tinder_auth_data["facebook_token"][0:-8]+"ndnhaxyn"

        write_log("Attempting auth with invalid token")
        req = requests.post(self.tinder_api_url+"auth",
                            headers=self.base_headers,
                            data=tinder_auth_data)
        write_log(req.text)

        write_log("Attempting auth with invalid user ID")
        tinder_auth_data = json.loads(facebookTokenAuth.construct_tinder_auth_post_data())
        tinder_auth_data["facebook_id"] = tinder_auth_data["facebook_id"][:-3]+"123"
        req = requests.post(self.tinder_api_url+"auth",
                            headers=self.base_headers,
                            data=tinder_auth_data)
        write_log(req.text)

    def get_updates(self):
        if not self.is_authorized():
            raise ValueError("Isn't authorized")

        url = self.tinder_api_url+"/updates"
        write_log("Getting updates")
        tokenized_headers = self.construct_tokenized_headers()
        req = requests.post(url, headers=tokenized_headers, data={"last_activity_date": ""})
        json_res = req.text
        parsed_res = json.loads(json_res)
        if "matches" in parsed_res:
            write_log("Got updates successfully")
            return parsed_res
        else:
            write_log("Failed getting updates")
            return parsed_res

    def get_recommendations(self, nth_try = 1, retries = 3):
        if not self.is_authorized():
            raise ValueError("Isn't authorized")

        url = self.tinder_api_url+"user/recs"
        tokenized_headers = self.construct_tokenized_headers()
        #write_log("Getting recommendations from URL {}\nwith headers {}".format(url, tokenized_headers))
        write_log("Getting recommendations...")
        req = requests.get(url, headers=tokenized_headers)
        json_res = req.text
        with open(self.rec_reqs_path, "a") as recjsonfile:
            recjsonfile.write("{}\n".format(json_res))
        parsed_res = json.loads(json_res)
        if "status" in parsed_res and parsed_res["status"] == 200:
            write_log("Got recommendations successfully")
            return parsed_res
        if "status" in parsed_res and parsed_res["status"] == 401:
            write_log("HTTP/1.1 401 Unauthorized.")
            if nth_try <= retries:
                write_log("Retrying after reauth")
                self.tinder_auth()
                return self.get_recommendations(nth_try+1, retries)
            else:
                write_log("Couldn't reauth. Sleeping for a day.")
                sleep(60*60*24)
                return parsed_res
        else:
            write_log("Failed getting recommendations")
            return parsed_res

    def construct_tokenized_headers(self):
        if self.tinder_auth_token is None:
            raise ValueError("Don't have a token to tokenize with!")
        base_headers_copy = self.base_headers.copy()
        base_headers_copy["X-Auth-Token"] = self.tinder_auth_token
        return base_headers_copy

    def print_user(self, user):
        if "name" not in user:
            user["name"] = "NOT_SET"
        if "_id" not in user:
            user["_id"] = "NOT_SET"
        if "birth_date" not in user:
            user["birth_date"] = "NOT_SET"
        if "distance_mi" not in user:
            user["distance_mi"] = -1
        if "photos" not in user:
            user["photos"] = []
        if "bio" not in user:
            user["bio"] = "NOT_SET"

        print("{} with _id '{}' born {}, {}mi away has {} photos, and a bio:\n{}\n{}".format(
            user["name"], user["_id"], user["birth_date"],
            user["distance_mi"], len(user["photos"]), user["bio"],
              "############"))


    def save_user(self, user_data_obj):
        user_own_json = user_data_obj["_id"]+".json"
        user_own_img_dir = self.imagedir+"/"+user_data_obj["_id"]
        if not os.path.exists(self.dbdir+"/"+user_own_json):
            os.makedirs(user_own_img_dir)
        json_data = json.dumps(user_data_obj)
        with open(self.dbdir+"/"+user_own_json, "w") as dbfilefd:
            dbfilefd.write(json_data)
        for image_obj in user_data_obj["photos"]:
            url = image_obj["url"]
            imgfile = user_own_img_dir+"/"+image_obj["id"]+".jpg"
            if not os.path.exists(imgfile):
                self.dl_image(url, imgfile)
            else:
                write_log("Skipping image, already in db")

    def has_user_photo(self, tinder_user_id, tinder_photo_id):
        try:
            return os.stat(self.build_photo_path(tinder_user_id,
                                                 tinder_photo_id)).st_size > 0
        except FileNotFoundError:
            return False

    def build_photo_path(self, tinder_user_id, tinder_photo_id):
        return "{}/{}/{}.jpg".format(self.imagedir,
                                     tinder_user_id,
                                     tinder_photo_id)


    def get_db_handle(self):
        conn = sqlite3.connect(DB_PATH)
        conn.row_factory = sqlite3.Row
        return conn

    def get_enum_user_ids_sql_template(self):
        with open("{}/enum_user_ids.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def load_user_ids(self):
        sql_template = self.get_enum_user_ids_sql_template()
        db = self.get_db_handle()
        db_cur = db.cursor()
        db_cur.execute(sql_template)
        rows = db_cur.fetchall()
        return rows

    def dl_image(self, url, savepath):
        req = requests.get(url, stream=True)
        if req.status_code == 200:
            with open(savepath, 'wb') as f:
                for chunk in req:
                    f.write(chunk)
            write_log("Successfully saved image {}".format(url))
            return True
        else:
            write_log("Error downloading image from url '{}' HTTP/1.1 {}".format(url, req.status_code))
            return False

    def exists_tinder_auth_token_file(self):
        if os.path.isfile(self.tinder_auth_token_file_path):
            return True
        else:
            return False

    def has_tinder_auth_token_in_cache(self):
        if self.exists_tinder_auth_token_file():
            token = self.read_tinder_auth_token_from_cache()
            if self.is_valid_tinder_auth_token(token):
                return True
            else:
                return False
        else:
            return False

    def read_tinder_auth_token_from_cache(self):
        with open(self.tinder_auth_token_file_path) as fd:
            token = fd.readline().replace("\n", "").replace(" ", "")
        if not self.is_valid_tinder_auth_token(token):
            raise AuthorizationError("Invalid fb auth token in file")
        return token

    def is_valid_tinder_auth_token(self, token):
        if token is None:
            return False
        if len(token.replace(" ", "").replace("\n", "")) < 1:
            return False
        else:
            return True

    def update_tinder_auth_token_cache(self, token):
        with open(self.tinder_auth_token_file_path, "w") as fd:
            fd.write(token)

    def valid_tinder_user_id(self, user_id):
        if user_id is None:
            return False
        if len(user_id.replace(" ", "")) < 1:
            return False
        return True

    def vote(self, user_id, type):
        if not self.is_authorized:
            raise UnauthorizedError("Voting requires to be authorized.")
        if not self.valid_tinder_user_id(user_id):
            raise ValueError("Invalid tinder user ID '{}'".format(user_id))
        res = None
        if type == 0:
            # Pass
            res = self.tinder_api_get_preauth("{}/{}".format("pass", user_id))
        elif type == 1:
            # Like
            res = self.tinder_api_get_preauth("{}/{}".format("like", user_id))
        else:
            raise ValueError("Only 0 (pass) and 1 (like) supported atm")
        write_log(json.dumps(res))
        if "likes_remaining" in res:
            self.likes_left = res["likes_remaining"]
        if "rate_limited_until" in res:
            """ TODO: fix likes left, api is lying about the amount, PoS"""
            self.rate_limited_until = res["rate_limited_until"]
            self.likes_left = 0
        return res["match"]

    def vote_l(self, user_id):
        return self.vote(user_id, 1)

    def vote_p(self, user_id):
        return self.vote(user_id, 0)

    def parse_local(self):
        with open(self.rec_reqs_path) as fd:
            for jsondata in fd:
                objformat = json.loads(jsondata)
                if "results" in objformat:
                    for userdata in objformat["results"]:
                        if "tinder_rate_limited" in userdata["_id"]:
                            continue
                        tinder_user = self.build_tinder_user_from_api_obj(userdata)
                        tinder_user.save_data()

    def run_continuous_spider(self):
        if self.is_authorized:
            while True:
                if self.get_recs_and_save() is False:
                    write_log("Couldn't get any recs, sleeping for an hour")
                    sleep(60*60)
                else:
                    print("### CTRL-C TO ABORT NOW!! ###")
                    sleep(5)

    def get_recs_and_save(self):
        is_ratelimited = False
        rec_obj = self.get_recommendations()
        if "message" in rec_obj:
            if rec_obj["message"] == "recs exhausted":
                write_log("Seems like there are no more users.")
                return False
            if rec_obj["message"] == "recs timeout":
                write_log("Seems like tinder doesn't want us to ask for recs in a while.")
                return False
            write_log("Message says '{}'".format(rec_obj["message"]))
        if "results" not in rec_obj:
            num_users = 0
            write_log("Got disturbing rec obj:")
            write_log(json.dumps(rec_obj))
        else:
            num_users = len(rec_obj["results"])
        write_log("Got {} users".format(num_users))
        if(num_users < 1):
            write_log("No more results. Closing down.")
            return False

        for user in rec_obj["results"]:
            if "tinder_rate_limited" in user["_id"]:
                write_log("Rate limit exceeded, cannot get recommendations atm")
                return False
            self.save_user(user)
            tinder_user = self.build_tinder_user_from_api_obj(user)
            tinder_user.save_data()
        return True

    def save_rec_from_file(self, filename):
        rec_obj = None
        with open(filename) as fd:
            rec_obj = json.load(fd)
        if "results" not in rec_obj:
            write_log("No results in rec obj")
            return False
        for user in rec_obj["results"]:
            if "tinder_rate_limited" in user["_id"]:
                write_log("No real users in results (rate limited)")
                break
            self.save_user(user)
            tinder_user = self.build_tinder_user_from_api_obj(user)
            tinder_user.save_data()

    def build_tinder_user_from_api_obj(self, api_user_data):
        tinder_user = TinderUser()
        if tinder_user.select_user_id_by_tinder_id(api_user_data["_id"]) is not None:
            tinder_user.load_with_tinder_id(api_user_data["_id"])
        instagram = {"photos": []}
        if "bio" not in api_user_data:
            api_user_data["bio"] = ""
        ig_user = None
        if "instagram" in api_user_data and api_user_data["instagram"] is not None:
            instagram = api_user_data["instagram"]
            ig_user = instagram["username"]
        bd_timestamp = dateutil.parser.parse(api_user_data["birth_date"]).timestamp()
        discovery_long = -1
        discovery_lat = -1
        tinder_user.set_user_data(None, api_user_data["_id"],
                                  api_user_data["name"],
                                  bd_timestamp,
                                  api_user_data["distance_mi"],
                                  discovery_long,
                                  discovery_lat,
                                  api_user_data["bio"],
                                  ig_user)
        for tinder_photo_obj in api_user_data["photos"]:
            photo_local_path = None
            if self.has_user_photo(api_user_data["_id"],
                                   tinder_photo_obj["id"]):
                photo_local_path = self.build_photo_path(api_user_data["_id"],
                                                         tinder_photo_obj["id"])
            tinder_user.add_photo(tinder_photo_obj["id"],
                                  tinder_photo_obj["url"],
                                  photo_local_path)
        if "photos" not in instagram:
            instagram["photos"] = []
        for ig_photo_obj in instagram["photos"]:
            tinder_user.add_instagram_photo(ig_photo_obj["image"])
        for job in api_user_data["jobs"]:
            company = None
            title = None
            if "company" in job:
                company = job["company"]["name"]
            if "title" in job:
                title = job["title"]["name"]
            tinder_user.add_work(company,
                                 title)
        for school in api_user_data["schools"]:
            school_id = None
            if "id" in school:
                school_id = int(school["id"])
            tinder_user.add_education(school_id, school["name"])

        return tinder_user

    def download_local(self):
        with open(self.rec_reqs_path) as fd:
            for jsondata in fd:
                objformat = json.loads(jsondata)
                if "results" not in objformat:
                    write_log("Couldnt't find results in jsondata '{}".format(jsondata))
                else:
                    for userdata in objformat["results"]:
                        self.print_user(userdata)
                        self.save_user(userdata)

    def get_meta(self):
        write_log("Getting meta")
        res = self.tinder_api_get_preauth("{}".format("meta"))
        return res

    def update_likes_left(self):
        meta_obj = self.get_meta()
        if meta_obj is not None and "status" in meta_obj and int(meta_obj["status"]) == 200:
            self.likes_left = int(meta_obj["rating"]["likes_remaining"])
            if self.likes_left < 1:
                self.rate_limited_until = time.time()+60*60*12*1000
            write_log("Likes remaining: {}".format(self.likes_left))
        else:
            write_log("Error getting meta for likes remaining.")
            self.likes_left = -1

    def init_sqlite(self):
        db = sqlite3.connect(DB_PATH)
        db_cursor = db.cursor()
        with open("{}/user.table.sql".format(INIT_DB_PATH)) as fd:
            db_cursor.execute(fd.readline())
        with open("{}/photo.table.sql".format(INIT_DB_PATH)) as fd:
            db_cursor.execute(fd.readline())
        with open("{}/vote.table.sql".format(INIT_DB_PATH)) as fd:
            db_cursor.execute(fd.readline())
        with open("{}/education.table.sql".format(INIT_DB_PATH)) as fd:
            db_cursor.execute(fd.readline())
        with open("{}/work.table.sql".format(INIT_DB_PATH)) as fd:
            db_cursor.execute(fd.readline())
        with open("{}/instagram_photo.table.sql".format(INIT_DB_PATH)) as fd:
            db_cursor.execute(fd.readline())
        db.commit()
        db.close()
        return None

    def has_database(self):
        if os.path.exists(DB_PATH):
            return True
        else:
            return False

    def get_db_template_user_ids(self):
        with open("{}/enum_user_ids.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_user_ids(self):
        sql_template = self.get_db_template_user_ids()
        db = self.get_db_handle()
        db_cursor = db.cursor()
        db_cursor.execute(sql_template)
        rows = db_cursor.fetchall()
        ids = []
        for row in rows:
            ids.append(row["id"])
        json.dumps(row["id"])
        return rows

    def get_sql_template_users_without_votes(self, with_limit = False):
        if with_limit == True:
            with open("{}/select_user_ids_without_votes_with_limit.sql".format(SQL_PATH)) as fd:
                return fd.readline()
        with open("{}/select_user_ids_without_votes.sql".format(SQL_PATH)) as fd:
            return fd.readline()

    def get_users_without_votes(self, num_limit = -1):
        with_limit = False
        if num_limit != -1:
            with_limit = True
        db = self.get_db_handle()
        db_cur = db.cursor()
        sql_template = self.get_sql_template_users_without_votes(with_limit)
        if with_limit:
            db_cur.execute(sql_template, {"max_users": num_limit})
        else:
            db_cur.execute(sql_template)
        rows = db_cur.fetchall()
        user_objs = []
        for user_id_row in rows:
            user_obj = TinderUser()
            user_obj.load_with_id(user_id_row["id"])
            user_objs.append(user_obj)
        return user_objs


    def run_likemachine(self):
        users_without_votes = self.get_users_without_votes(100)
        for user in users_without_votes:
            if self.likes_left < 1:
                write_log("Like limit reached, stopping the likemachine.")
                break;
            write_log("Liking {} who is {} miles away...".format(user.get_name(),
                                                                 user.get_distance_mi()))
            vote_res = self.vote_l(user.get_tinder_id())
            if vote_res is not False:
                write_log("############")
                write_log("############")
                write_log("WAS A MATCH!")
                write_log("############")
                write_log("############")
                user.mail_user()
            else:
                write_log("Not a match.")
            user.insert_vote(1)
            sleep(1)

    def rec_like_machine(self):
        while True:
            self.update_likes_left()
            self.run_likemachine()
            if self.likes_left < 1 or self.get_recs_and_save() is False:
                write_log("Sleeping for an hour")
                sleep(60*60)


    def ping(self, lat, lon):
        print(json.dumps(self.tinder_api_post_preauth("user/ping", {"lat": float(lat),
                                                   "lon": float(lon)})))


class TinderUser:

    def __init__(self):
        self.db = None
        self.data = {
            'user': None,
            'education': [],
            'instagram_photo': [],
            'photo': [],
            'work': [],
            'vote': []
        }

    def get_db_handle(self):
        if self.db is None:
            self.db = sqlite3.connect(DB_PATH)
            self.db.row_factory = sqlite3.Row
            cur = self.db.cursor()
            cur.execute("PRAGMA foreign_keys = ON;")
        return self.db

    def get_db_insert_user_template(self):
        with open("{}/insert_user.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_insert_instagram_photo_template(self):
        with open("{}/insert_instagram_photo.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_insert_tinder_photo_template(self):
        with open("{}/insert_photo.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_insert_education_template(self):
        with open("{}/insert_education.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_insert_work_template(self):
        with open("{}/insert_work.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_insert_vote_template(self):
        with open("{}/insert_vote.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_update_user_template(self):
        with open("{}/update_user.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_update_instagram_photo_template(self):
        with open("{}/update_instagram_photo.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_update_tinder_photo_template(self):
        with open("{}/update_photo.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_update_education_template(self):
        with open("{}/update_education.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_update_work_template(self):
        with open("{}/update_work.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_update_vote_template(self):
        with open("{}/update_vote.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_delete_instagram_photo_template(self):
        with open("{}/delete_instagram_photo.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_delete_tinder_photo_template(self):
        with open("{}/delete_photo.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_delete_education_template(self):
        with open("{}/delete_education.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_delete_work_template(self):
        with open("{}/delete_work.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_delete_vote_template(self):
        with open("{}/delete_vote.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_select_user_id_by_tinder_id(self):
        with open("{}/get_user_id_by_tinder_id.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def select_user_id_by_tinder_id(self, tinder_id):
        sql_template = self.get_db_select_user_id_by_tinder_id();
        db = self.get_db_handle()
        cur = db.cursor()
        cur.execute(sql_template, {"tinder_id": tinder_id})
        rows = cur.fetchall()
        if len(rows) < 1:
            write_log("Couldn't find userid for {}".format(tinder_id))
            return None
        return rows[0]["id"]


    def insert_user_db(self, tinder_id, name, birth_date_timestamp,
                          distance_mi, discovery_long, discovery_lat,
                          bio, instagram_username, autocommit = False):
        sql_template = self.get_db_insert_user_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": None,
                                   "tinder_id": tinder_id,
                                   "name": name,
                                   "birth_date_timestamp": birth_date_timestamp,
                                   "distance_mi": distance_mi,
                                   "discovery_long": discovery_long,
                                   "discovery_lat": discovery_lat,
                                   "bio": bio,
                                   "instagram_username": instagram_username})
        if autocommit:
            db.commit()
        #db.close()
        return cur.lastrowid

    def update_user_db(self, user_id, tinder_id, name, birth_date_timestamp,
                            distance_mi, discovery_long, discovery_lat,
                            bio, instagram_username, autocommit = False):
        sql_template = self.get_db_update_user_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": user_id,
                                   "tinder_id": tinder_id,
                                   "name": name,
                                   "birth_date_timestamp": birth_date_timestamp,
                                   "distance_mi": distance_mi,
                                   "discovery_long": discovery_long,
                                   "discovery_lat": discovery_lat,
                                   "bio": bio,
                                   "instagram_username": instagram_username})
        if autocommit:
            db.commit()
        #db.close()

    def delete_user_db(self, user_id, autocommit = False):
        sql_template = self.get_db_delete_user_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": user_id})
        if autocommit:
            db.commit()
        #db.close()

    def insert_education_db(self, user_id, tinder_school_id,
                            institution, autocommit = False):
        sql_template = self.get_db_insert_education_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": None,
                                   "user_id": user_id,
                                   "tinder_school_id": tinder_school_id,
                                   "institution": institution})
        if autocommit:
            db.commit()
        #db.close()
        return cur.lastrowid

    def update_education_db(self, db_id, user_id, tinder_school_id,
                            institution, autocommit = False):
        sql_template = self.get_db_update_education_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": db_id,
                                   "user_id": user_id,
                                   "tinder_school_id": tinder_school_id,
                                   "institution": institution})
        if autocommit:
            db.commit()
        #db.close()

    def delete_education_db(self, id, autocommit = False):
        sql_template = self.get_db_delete_education_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": id})
        if autocommit:
            db.commit()
        #db.close()

    def insert_instagram_photo_db(self, user_id, url, autocommit = False):
        sql_template = self.get_db_insert_instagram_photo_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": None,
                                   "user_id": user_id,
                                   "url": url})
        if autocommit:
            db.commit()
        #db.close()
        return cur.lastrowid

    def update_instagram_photo_db(self, db_id, user_id, url, autocommit = False):
        sql_template = self.get_db_update_instagram_photo_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": db_id,
                                   "user_id": user_id,
                                   "url": url})
        if autocommit:
            db.commit()
        #db.close()

    def delete_instagram_photo_db(self, id, autocommit = False):
        sql_template = self.get_db_delete_instagram_photo_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": id})
        if autocommit:
            db.commit()
        #db.close()

    def insert_photo_db(self, user_id, tinder_photo_id, url, local_path,
                        autocommit = False):
        sql_template = self.get_db_insert_tinder_photo_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": None,
                                   "user_id": user_id,
                                   "tinder_photo_id": tinder_photo_id,
                                   "url": url,
                                   "local_path": local_path})
        if autocommit:
            db.commit()
        #db.close()
        return cur.lastrowid

    def update_photo_db(self, db_id, user_id, tinder_photo_id, url, local_path,
                        autocommit = False):
        sql_template = self.get_db_update_tinder_photo_template()
        db = self.get_db_handle()
        cur = db.cursor()
        cur.execute(sql_template, {"id": db_id,
                                   "user_id": user_id,
                                   "tinder_photo_id": tinder_photo_id,
                                   "url": url,
                                   "local_path": local_path})
        if autocommit:
            db.commit()
        #db.close()

    def delete_photo_db(self, id, autocommit = False):
        sql_template = self.get_db_delete_photo_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": id})
        if autocommit:
            db.commit()
        #db.close()

    def insert_vote_db(self, timestamp, value, autocommit = True):
        sql_template = self.get_db_insert_vote_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": None,
                                   "user_id": self.data["user"]["db_id"],
                                   "timestamp": timestamp,
                                   "value": value})
        if autocommit:
            db.commit()
        #db.close()
        return cur.lastrowid

    def update_vote_db(self, db_id, timestamp, value, autocommit = False):
        sql_template = self.get_db_update_vote_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": db_id,
                                   "timestamp": timestamp,
                                   "value": value})
        if autocommit:
            db.commit()
        #db.close()

    def delete_vote_db(self, id, autocommit = False):
        sql_template = self.get_db_delete_vote_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": id})
        if autocommit:
            db.commit()
        #db.close()

    def insert_work_db(self, user_id, company, position, autocommit = False):
        sql_template = self.get_db_insert_work_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": None,
                                   "user_id": user_id,
                                   "company": company,
                                   "position": position})
        if autocommit:
            db.commit()
        #db.close()
        return cur.lastrowid

    def update_work_db(self, db_id, user_id, company, position, autocommit = False):
        sql_template = self.get_db_update_work_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": db_id,
                                   "user_id": user_id,
                                   "company": company,
                                   "position": position})
        if autocommit:
            db.commit()
        #db.close()

    def delete_work_db(self, id, autocommit = False):
        sql_template = self.get_db_delete_work_template()
        db = self.get_db_handle()
        cur = db.cursor()
        #cur.execute("PRAGMA foreign_keys = ON;")
        cur.execute(sql_template, {"id": id})
        if autocommit:
            db.commit()
        #db.close()

    def set_user_data(self, db_id, tinder_id, name, birth_date_timestamp,
                      distance_mi, discovery_long, discovery_lat, bio,
                      instagram_username):
        self.data["user"] = {"db_id": db_id,
                             "tinder_id": tinder_id, "name": name,
                             "birth_date_timestamp": birth_date_timestamp,
                             "distance_mi": distance_mi,
                             "discovery_long": discovery_long,
                             "discovery_lat": discovery_lat,
                             "bio": bio,
                             "instagram_username": instagram_username}

    def add_work(self, company, position):
        work_obj = {"db_id": None,
                    "company": company,
                    "position": position}
        for work in self.data["work"]:
            if work["company"] == company and work["position"] == position:
                return
        self.data["work"].append(work_obj)

    def add_education(self, tinder_school_id, institution):
        '''write_log("Adding education {} with id {} for user {}".format(institution,
                                                                      tinder_school_id,
                                                                      self.data["user"]["db_id"]))'''
        edu_obj = {"db_id": None,
                   "tinder_school_id": tinder_school_id,
                   "institution": institution}
        for edu in self.data["education"]:
            if edu["tinder_school_id"] == tinder_school_id and edu["institution"] == institution:
                return
        self.data["education"].append(edu_obj)

    def add_photo(self, tinder_photo_id, url, local_path):
        for photo in self.data["photo"]:
            if photo["tinder_photo_id"] == tinder_photo_id:
                return
        self.data["photo"].append({"db_id": None,
                                   "tinder_photo_id": tinder_photo_id,
                                   "url": url,
                                   "local_path": local_path})

    def add_instagram_photo(self, url):
        for ig_photo in self.data["instagram_photo"]:
            if ig_photo["url"] == url:
                return
        self.data["instagram_photo"].append({"db_id": None,
                                             "url": url})

    def save_user_data(self):
        #print("Saving user data...")
        user_data = self.data["user"]
        if user_data["db_id"] is None:
            """ Try and find it """
            maybe_id = self.select_user_id_by_tinder_id(self.data["user"]["tinder_id"])
            if maybe_id is not None:
                user_data["db_id"] = maybe_id
        if user_data["db_id"] is not None:
            """ UPDATE """
            self.update_user_db(user_data["db_id"], user_data["tinder_id"],
                                user_data["name"],
                                user_data["birth_date_timestamp"],
                                user_data["distance_mi"],
                                user_data["discovery_long"],
                                user_data["discovery_lat"],
                                user_data["bio"],
                                user_data["instagram_username"])
        else:
            """ INSERT """
            id = self.insert_user_db(user_data["tinder_id"],
                                user_data["name"],
                                user_data["birth_date_timestamp"],
                                user_data["distance_mi"],
                                user_data["discovery_long"],
                                user_data["discovery_lat"],
                                user_data["bio"],
                                user_data["instagram_username"])
            self.data["user"]["db_id"] = id
        #print("Done.")

    def save_work_datas(self):
        #print("Saving work datas...")
        user_id = self.data["user"]["db_id"]
        if user_id is None:
            raise ValueError("Cannot save work if user has no ID")

        work_datas = self.data["work"]
        for work_data in work_datas:
            if work_data["db_id"] is None:
                """ INSERT """
                id = self.insert_work_db(user_id,
                                    work_data["company"],
                                    work_data["position"])
                work_data["db_id"] = id
            else:
                """ UPDATE """
                print(json.dumps(work_data))
                self.update_work_db(work_data["db_id"],
                                    user_id,
                                    work_data["company"],
                                    work_data["position"])
        #print("Done.")

    def save_education_datas(self):
        #print("Saving education data...")
        user_id = self.data["user"]["db_id"]
        if user_id is None:
            raise ValueError("Cannot save education if user has no ID")

        education_datas = self.data["education"]
        for education_data in education_datas:
            if education_data["db_id"] is None:
                """ INSERT """
                id = self.insert_education_db(user_id,
                                         education_data["tinder_school_id"],
                                         education_data["institution"])
                education_data["db_id"] = id
            else:
                """ UPDATE """
                self.update_education_db(education_data["db_id"],
                                         user_id,
                                         education_data["tinder_school_id"],
                                         education_data["institution"])
        #print("Done.")

    def save_photo_datas(self):
        #print("Saving photo datas...")
        user_id = self.data["user"]["db_id"]
        if user_id is None:
            raise ValueError("Cannot save photo if user has no ID")

        photo_datas = self.data["photo"]
        for photo_data in photo_datas:
            if photo_data["db_id"] is None:
                """ INSERT """
                id = self.insert_photo_db(user_id,
                                     photo_data["tinder_photo_id"],
                                     photo_data["url"],
                                     photo_data["local_path"])
                photo_data["db_id"] = id
            else:
                self.update_photo_db(photo_data["db_id"],
                                     user_id,
                                     photo_data["tinder_photo_id"],
                                     photo_data["url"],
                                     photo_data["local_path"])
        #print("Done.")

    def save_instagram_photo_datas(self):
        #print("Saving instagram photo datas...")
        user_id = self.data["user"]["db_id"]
        if user_id is None:
            raise ValueError("Cannot save ig photo if user has no ID")

        instagram_photo_datas = self.data["instagram_photo"]
        for ig_photo_data in instagram_photo_datas:
            if ig_photo_data["db_id"] is None:
                """ INSERT """
                id = self.insert_instagram_photo_db(user_id,
                                               ig_photo_data["url"])
                ig_photo_data["db_id"] = id
            else:
                """ UPDATE """
                self.update_instagram_photo_db(ig_photo_data["db_id"],
                                               user_id,
                                               ig_photo_data["url"])
        #print("Done.")


    def save_data(self):
        print("Saving data for {} ({})".format(self.data["user"]["name"],
                                                   self.data["user"]["tinder_id"]))
        self.save_user_data()
        self.save_education_datas()
        self.save_instagram_photo_datas()
        self.save_photo_datas()
        self.save_work_datas()
        self.db.commit()
        #print("Done.")

    def insert_vote(self, value):
        if value not in [0, 1]:
            raise ValueError("Value has to be 0=pass or 1=like")
        self.insert_vote_db(int(time.time()),
                            value)

    def get_db_select_user(self):
        with open("{}/get_user_data.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_select_user_photos(self):
        with open("{}/select_photos_for_user.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_select_user_instagram_photos(self):
        with open("{}/select_instagram_photos_for_user.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_select_user_educations(self):
        with open("{}/select_educations_for_user.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def get_db_select_user_works(self):
        with open("{}/select_works_for_user.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False

    def add_raw_photo(self, id, tinder_photo_id, url, local_path):
        self.data["photo"].append({"db_id": id,
                                   "tinder_photo_id": tinder_photo_id,
                                   "url": url,
                                   "local_path": local_path})

    def add_raw_instagram_photo(self, id, url):
        self.data["instagram_photo"].append({"db_id": id,
                                             "url": url})

    def add_raw_education(self, id, tinder_school_id, institution):
        self.data["education"].append({"db_id": id,
                                  "tinder_school_id": tinder_school_id,
                                  "institution": institution})

    def add_raw_work(self, id, company, position):
        self.data["work"].append({"db_id": id,
                                  "company": company,
                                  "position": position})


    def load_user_data(self, id):
        db = self.get_db_handle()
        db_cursor = db.cursor()
        user_data_sql_template = self.get_db_select_user()
        db_cursor.execute(user_data_sql_template, {"id": id})
        row = db_cursor.fetchone()
        self.set_user_data(row["id"], row["tinder_id"], row["name"],
                           row["birth_date_timestamp"],
                           row["distance_mi"], row["discovery_long"],
                           row["discovery_lat"], row["bio"],
                           row["instagram_username"])

    def load_photos(self):
        user_photos_sql_template = self.get_db_select_user_photos()
        db = self.get_db_handle()
        db_cursor = db.cursor()
        db_cursor.execute(user_photos_sql_template, {"user_id": self.data["user"]["db_id"]})
        user_photos = db_cursor.fetchall()
        for user_photo_row in user_photos:
            self.add_raw_photo(user_photo_row["id"],
                               user_photo_row["tinder_photo_id"],
                               user_photo_row["url"],
                               user_photo_row["local_path"])

    def load_instagram_photos(self):
        instagram_photo_sql_template = self.get_db_select_user_instagram_photos()
        db = self.get_db_handle()
        db_cursor = db.cursor()
        db_cursor.execute(instagram_photo_sql_template, {"user_id": self.data["user"]["db_id"]})
        user_instagram_photos = db_cursor.fetchall()
        for user_instagram_photo_row in user_instagram_photos:
            self.add_raw_instagram_photo(user_instagram_photo_row["id"],
                                         user_instagram_photo_row["url"])

    def load_educations(self):
        user_educations_sql_template = self.get_db_select_user_educations()
        db = self.get_db_handle()
        db_cursor = db.cursor()
        db_cursor.execute(user_educations_sql_template, {"user_id": self.data["user"]["db_id"]})
        user_educations = db_cursor.fetchall()
        for user_education_row in user_educations:
            self.add_raw_education(user_education_row["id"],
                                   user_education_row["tinder_school_id"],
                                   user_education_row["institution"])

    def load_works(self):
        user_work_sql_template = self.get_db_select_user_works()
        db = self.get_db_handle()
        db_cursor = db.cursor()
        db_cursor.execute(user_work_sql_template, {"user_id": self.data["user"]["db_id"]})
        user_works = db_cursor.fetchall()
        for user_work_row in user_works:
            self.add_raw_work(user_work_row["id"],
                              user_work_row["company"],
                              user_work_row["position"])

    def get_db_has_user(self):
        with open("{}/has_user.sql") as fd:
            return fd.readline()
        return False

    def has_user(self, tinder_id):
        has_user_sql_template = self.get_db_has_user()
        db = self.get_db_handle()
        db_cursor = db.cursor()
        db_cursor.execute(has_user_sql_template, {"tinder_id": tinder_id})
        row = db_cursor.fetchone()
        if int(row["num"]) > 0:
            return True
        return False

    def get_db_get_user_id_by_tinder_id(self):
        with open("{}/get_user_id_by_tinder_id.sql".format(SQL_PATH)) as fd:
            return fd.readline()
        return False


    def load_with_tinder_id(self, tinder_id):
        sql_template = self.get_db_get_user_id_by_tinder_id()
        db = self.get_db_handle()
        db_cur = db.cursor()
        db_cur.execute(sql_template, {"tinder_id": tinder_id})
        row = db_cur.fetchone()
        self.load_with_id(row["id"])

    def load_with_id(self, id):
        self.load_user_data(id)
        self.load_photos()
        self.load_instagram_photos()
        self.load_educations()
        self.load_works()

    def dump_json(self):
        write_log(json.dumps(self.data, indent=4))

    def get_tinder_id(self):
        return self.data["user"]["tinder_id"]

    def get_name(self):
        return self.data["user"]["name"]

    def get_distance_mi(self):
        return self.data["user"]["distance_mi"]

    def mail_user(self):
        write_log("Mailing user {}".format(self.data["user"]["name"]))
        photo_paths = []
        for photo in self.data["photo"]:
            if photo["local_path"] is not None:
                photo_paths.append(photo["local_path"])

        insta = None
        if self.data["user"]["instagram_username"] is not None:
            insta = "https://instagr.am/{}/".format(self.data["user"]["instagram_username"])
        educations = ""
        if len(self.data["education"]) < 1:
            educations = "No education to display.\n"
        else:
            for education in self.data["education"]:
                educations += "School: " + education["institution"] + "\n"
        jobs = ""
        if len(self.data["work"]) < 1:
            jobs = "No work to display.\n"
        else:
            for work in self.data["work"]:
                jobs += (work["position"] if work["position"] is not None else "Unspecified")
                jobs += " at " + (work["company"] if work["company"] is not None else "unknown") + "\n"

        send_mail_with_images("Introducing {}".format(self.data["user"]["name"]),
                              "Name: {}\nAge: {}\nDistance: {}km\nSchool(s):\n{}Job(s):\n{}Instagram: {}\n\n{}".format(self.data["user"]["name"],
                              int((time.time()-self.data["user"]["birth_date_timestamp"])/60/60/24/365),
                              round(float(self.data["user"]["distance_mi"])*1.609344, 1),
                              educations,
                              jobs,
                              insta,
                              self.data["user"]["bio"]),
                 photo_paths)


"""
app = SpindRApp()

if len(sys.argv) > 1:
    cmd = sys.argv[1]
    if cmd == "get_recs":
        if not app.is_authorized():
            if app.tinder_auth():
                app.get_recs_and_save()
            else:
                print("Couldn't auth with tinder.")
    elif cmd == "parse_local":
        app.parse_local()
    elif cmd == "download_local":
        app.download_local()
    else:
        print("Didn't understand command '{}'".format(cmd))
else:
    print("Please supply command")
"""

CREATE TABLE IF NOT EXISTS education (id INTEGER PRIMARY KEY, user_id INTEGER, tinder_school_id INTEGER, institution TEXT, FOREIGN KEY(user_id) REFERENCES user(id) ON DELETE CASCADE);

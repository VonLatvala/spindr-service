import os
import smtplib
import json
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

def send_mail_with_images(subject, message, imgs):
    smtp_creds = None
    mail_sender = None
    mail_recipient = None
    with open("config/smtpCreds.json") as fd:
        smtp_creds = json.load(fd)
    with open("config/mailSender.json") as fd:
        mail_sender = json.load(fd)
    with open("config/mailRecipient.json") as fd:
        mail_recipient = json.load(fd)

    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg['From'] = "{} <{}>".format(mail_sender["name"], mail_sender["email"])
    msg['To'] = "{} <{}>".format(mail_recipient["name"], mail_recipient["email"])

    text = MIMEText(message)
    msg.attach(text)
    for img in imgs:
        img_data = open(img, 'rb').read()
        image = MIMEImage(img_data, name=os.path.basename(img), _subtype="jpeg")
        msg.attach(image)

    s = smtplib.SMTP(smtp_creds["host"], smtp_creds["port"])
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(smtp_creds["user"], smtp_creds["password"])
    s.sendmail(mail_sender["email"], mail_recipient["email"], msg.as_string())

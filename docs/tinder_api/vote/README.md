## LIKE / PASS

Accessing the API with the path `/like/{_id}` or `/pass/{_id}` can yield different kinds of results.  

When using the `like` path, on a non-match, an object like this is provided:  

```json
{
    "likes_remaining": 100,
    "match": false
}
```

When using the `like` path, on a match, an object like this is provided:  
```json
{
    "likes_remaining": 100,
    "match": {
        "closed": false,
        "common_friend_count": 0,
        "common_like_count": 0,
        "created_date": "1970-01-13T13:00:00.000Z",
        "dead": false,
        "following": true,
        "following_moments": true,
        "is_boost_match": false,
        "is_fast_match": false,
        "is_super_like": false,
        "last_activity_date": "1970-01-13T13:00:00.000Z",
        "message_count": 0,
        "messages": [],
        "participants": [
            "{_id}",
            "{_id}"
        ],
        "pending": false,
        "_id": "{_id}{_id}"
    }
}
```

This is assuming user has likes to use left. In case user has hit his rate limit, an object like this will be returned:  
```json
{
    "rate_limited_until": 1497997799686,
    "likes_remaining": 0,
    "match": false
}
```
  
As one can observe, the "match" item in the result object is either Boolean or Object, depending on if the match was a success. On success, "match" contains an Object with information about the match, and on mismatch, "match" contains a boolean (false).

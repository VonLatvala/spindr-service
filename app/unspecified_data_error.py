class UnspecifiedDataError(Exception):
    def __init__(self, message, errors):
        super(UnspecifiedDataError, self).__init__(message)

        self.errors = errors
